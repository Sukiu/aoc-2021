f = open("input.txt","r")
lines = f.readlines()
lines = [line[:-1] for line in lines]

cnt = 0
idx = 0
prev = int(lines[0])

for line in lines:
    if int(line) > prev:
        cnt = cnt+1
    idx=idx+1
    prev = int(lines[idx-3])
print(cnt)